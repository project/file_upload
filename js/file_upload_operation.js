(function($){
	var $operation_form;
	Drupal.behaviors.file_upload = {
		'attach' : function(context, settings){
			$operation_form = ($operation_form == undefined)?  $('#file-operation-form') : $operation_form;
			$('.operation').once('operation_click_attach', function(index){
				$(this).click(function(event){
					var fid = $(this).attr('data_fid');
					var action_type = $(this).attr('data_action_type');
					if(!$(this).hasClass('operation-processed')){
						$pre = $('.operation-processed')
						if($pre.length > 0){
							$pre.removeClass('operation-processed');
							
						}
						$(this).addClass('operation-processed');


						if(!$(this).parents('.row').hasClass('row-operation-processed')){
							$pre_row = $('.row-operation-processed');
							if( $pre_row.length > 0 ){
								$pre_row.removeClass('row-operation-processed');
								$pre_row.find('.link').show();
								
							}
							$(this).parents('.row').addClass('row-operation-processed');
							$('.link-' + fid).after($operation_form);
						}

						$operation_form.find('.file-operation-fid').val(fid);
						$operation_form.find('.file-operation-type').val(action_type);

						$operation_form.find('.file-operation-type').trigger('change');
						$operation_form.removeClass('element-invisible');
						if(action_type == 'rename'){
							$('.link-' + fid).hide();
						}else{
							$('.link-' + fid).show();
						}
						
					}
					event.preventDefault();
					return false;

				});
			});
			$('#cancel-button').once('cancel_click_attach',function(){
				$(this).click(function(event){
					$('.operation-processed').removeClass('operation-processed');
					$('.row-operation-processed').removeClass('row-operation-processed');
					$(this).parents('.row').find('.link').show();
					$operation_form.detach();
					event.preventDefault();
				});
				
			});
		
			$("input[name='path_view']").once('uri_change_attach', function(){
				$(this).mousedown(function(event){
					if($(this).is(':checked') == false){
						$('.form-managed-file').find('input').attr({'disabled':'disabled'});
					}
				});

			})





		}

	}
})(jQuery);