<?php
require_once dirname(__FILE__) . '/file_upload.functions.inc';
/**
* @file 
* File Upload managemend and configuration form
*
*/
function file_upload_management() {
 $header = array(t('Thumbnail'), t('User'), t('Size'), t('Time'), t('Url'), t('Operation'));
 $query = db_select('file_managed', 'fm')->extend('pagerDefault');
     $query->join('file_usage', 'fu', 'fu.fid = fm.fid');
     $files = $query->fields('fm')
        ->condition('module','file_upload')
        ->orderBy('fid', 'DESC')
        ->limit(30)
        ->execute()
        ->fetchAllAssoc('fid');
     $rows = file_upload_view_rows($files);

 return array(
  '#type' => 'container',
  'uri_addition_form' => drupal_get_form('file_upload_uri_addition_form'),
  'file_upload_form' => drupal_get_form('file_upload_upload_form'),
  'file_operation_form' => drupal_get_form('file_upload_operation_form'),
  'file_view_table' => array(
     '#theme' => 'table',
     '#header' => $header,
     '#rows' => $rows,
     '#attached' => array(
      'js' => array(drupal_get_path('module', 'file_upload') . '/js/file_upload_operation.js')),
     '#empty' => t('You Have not Upload File Yet!'), 
  ),
  'pager' => array(
   '#theme' => 'pager',
  ),
  '#attached' => array(
   'css' => array(drupal_get_path('module', 'file_upload') . '/css/file_upload.css'),
  ),
 );
}

function file_upload_upload_form($form, &$form_state) {
 global $base_url;
 $uris = variable_get('file_upload_path', array());
 $key_default = variable_get('file_upload_path_default', '');
 if(isset($form_state['values']['path_view'])){
   $key_select = $form_state['values']['path_view'];
   if ($key_default != $key_select) {
       variable_set('file_upload_path_default', $key_select);
       $key_default = $key_select;
   }
 }
  
 $uri = empty($uris)? 'public://file_upload' : $uris[$key_default]['schema'] . '://' . $uris[$key_default]['path'];
 $allow_extensions = variable_get('file_upload_extensions', '');
 $allow_extensions = str_replace(',', ' ', $allow_extensions);
  $form['wrapper'] = array(
      '#type' => 'fieldset',
      '#title' => t('File upload'),
      'file_upload_fid' => array(
          '#title' => t('File upload'),
          '#description' => t('The file Will Upload to the location "@url"', array('@url' => str_replace($base_url, '', file_create_url($uri)) )),
          '#type' => 'managed_file',
          '#default_value' =>  0,
          '#upload_validators' => array('file_validate_extensions' => array($allow_extensions)),
          '#upload_location' => $uri,
          '#weight' => 1,

      ),

   );

  $header = array(
    'uri' => t('URI'),
    'op' => t('Operaions'),
   );

 $options = array();
 if (!empty($uris)) {
  foreach ($uris as $key => $uri) {
   $options[$key] = array(
    'uri' => $uri['schema'] . '://' . $uri['path'],
    'op' => l(t('Delete'),'file_upload_path/delete/' . $key . '/no-js', array('attributes' => array('class' => array('use-ajax')))),
    '#attributes' => array('class' => array('path-row-' . $key)),

   );
  }
  $options[$key_default]['op'] = '';
 }

  $theme_default = variable_get('theme_default', '');
  $form['wrapper']['path_view'] = array(
   '#type' => 'tableselect',
   '#header' => $header,
   '#options' => $options,
   '#multiple' => FALSE,
   '#default_value' => $key_default,
   '#empty' => t('No Data'),
   '#ajax' => array(
      'callback' => 'file_upload_uri_change_callback',
      'progress' => array('type' => null),
    ),
   '#prefix' => '<div id="file-upload-uri-view">',
   '#suffix' => '</div>',
   '#weight' => -2,
  );
  $form['wrapper']['submit'] = array(
   '#type' => 'submit',
   '#value' => t('Save'),
   '#weight' => 2,
   '#attributes' => array(
    'class' => array('file-upload-submit'),
   ),
  );


  return $form;
}

function file_upload_uri_change_callback($form, &$form_state){

  $commands[] = ajax_command_replace('#edit-file-upload-fid-ajax-wrapper', drupal_render($form['wrapper']['file_upload_fid']));
  $commands[] = ajax_command_html('#file-upload-uri-view', drupal_render($form['wrapper']['path_view']));
  return array('#type' => 'ajax', '#commands' => $commands);

}
function file_upload_operation_form($form, &$form_state) {
   $operation_type = isset($form_state['values']['file_operation_type'])? $form_state['values']['file_operation_type'] : '';
   
   if (isset($form_state['input']['file_operation_name'])) {
     unset($form_state['input']['file_operation_name']);
   }
   if (isset($form_state['input']['file_operation_file'])) {
     unset($form_state['input']['file_operation_file']);
   }
   $action_text = array(
      '' => array('note' => '', 'button' => 'Save'),
      'replace' => array('note' => t('The Source File You Upload should Have the Same MIME Type of the Destination.'), 'button' => t('Replace')),
      'rename' => array('note' => t('The Name of the File You Upload should not as Same as Existent Ones.'), 'button' => t('Rename')),
      'delete' => array('note' => t('Are You Sure to Delete this File?'), 'button' => t('Yes')),
    );
    $form['file_operation_placeholder'] = array(
     '#type' => 'markup',
     '#markup' => '<div id="file-operation-placeholder"></div>',
    );
    switch ($operation_type) {
     case 'replace':
     $form['file_operation_file'] = array(
      '#type' => 'file',
      '#prefix' => '<div id="file-operation-placeholder">',
      '#suffix' => $action_text[$operation_type]['note'] . '</div>',
     );
     break;
     case 'rename':
     $file = file_load($form_state['values']['file_operation_fid']);
     $url_commponents  = pathinfo(file_create_url($file->uri));
     $form['file_operation_name'] = array(
      '#type' => 'textfield',
      '#size' => 20,
      '#maxlength' => 120,
      '#required' => TRUE,
      '#default_value' => $url_commponents['filename'],
      '#field_prefix' => $url_commponents['dirname'] . '/',
      '#field_suffix' => '.' .$url_commponents['extension'],
      '#prefix' => '<div id="file-operation-placeholder">',
      '#suffix' => $action_text[$operation_type]['note'] . '</div>',
     );
     $form_state['file_upload_filename'] = $url_commponents['filename'];
     break;
     case 'delete':
     $form['markup'] = array(
        '#markup' => '<div id="file-operation-placeholder">' . $action_text[$operation_type]['note'] . '</div>',
      );
     break;
    }
    
    $form['file_operation_fid'] = array(
      '#type' => 'hidden',
      '#default_value' => '',
      '#attributes' => array('class' => array('file-operation-fid')),
    );
   $form['file_operation_type'] = array(
     '#type' => 'select',
     '#options' => array('delete' => 'Delete', 'replace' => 'Replace', 'rename' => 'Rename'),
     '#required' => TRUE,
     '#attributes' => array('class' => array('file-operation-type', 'element-invisible')),
     '#ajax' => array(
      'callback' => 'file_operation_callback',
      'progress' => array('type' => NULL),
     ),
    );

    $form['actions'] = array(
     '#type' => 'actions',
     '#attributes' => array('id' => 'file-upload-actions'),
     '#weight' => 10,
     'submit' => array(
      '#type' => 'submit',
      '#submit' => array('file_' . $operation_type . '_submit'),
      '#validate' => array('file_default_validate', 'file_' . $operation_type . '_validate' ),
      '#ajax' => array(
        'callback' => 'file_submit_callback',
        'progress' => array('type' => NULL),
        ),
      '#value' => $action_text[$operation_type]['button'],

     ),
     'cancel' => array(
      '#type' => 'button',
      '#value' => t('Cancel'),
      '#attributes' => array('id' => 'cancel-button'),
     ),

    );


    $form['#attributes'] = array('id' => 'file-operation-form', 'class' => array('element-invisible'), 'enctype' => 'multipart/form-data');

    return $form;
 }
function file_operation_callback($form, $form_state){
  
  $rndr = function($element_id) use ($form){
    if(isset($form[$element_id])){
      return drupal_render($form[$element_id]);
    }
    return $element_id;
  };
  $fid = $form_state['values']['file_operation_fid'];
  $operation_type = $form_state['values']['file_operation_type'];
  $replacement['replace'] = 'file_operation_file';
  $replacement['rename'] = 'file_operation_name';
  $replacement['delete'] = 'markup';

  $commands[] = ajax_command_replace('#file-upload-actions',drupal_render($form['actions']));
  $commands[] = ajax_command_replace('#file-operation-placeholder', $rndr($replacement[$operation_type]));
  return array('#type' => 'ajax', '#commands' => $commands);
}
function file_default_validate($form, &$form_state){
   $fid = $form_state['values']['file_operation_fid'];
   if(!($form_state['file_upload_dest'] = file_load($fid))){
      form_set_error('file_operation_fid', t('Failed to Load the Destination file you operate!'));
   }
}
function file_replace_validate($form, &$form_state) {

 $dest = $form_state['file_upload_dest'];
 $source = file_save_upload('file_operation_file', array(), FALSE, FILE_EXISTS_RENAME);
 $form_state['file_upload_source'] = $source;
 if (!$source) {
        form_set_error('file_operation_file', t('Please select a correct file!'));
 }elseif ($source->filemime != $dest->filemime) {
   form_set_error('file_operation_file', t('You Can\'t Replace a File With File has Different MIME Type!')); 
 }
  
}
function file_replace_submit($form, &$form_state) {
  if (form_get_errors()) return;
 $dest = $form_state['file_upload_dest'];
 $source = $form_state['file_upload_source'];
 file_unmanaged_copy($source->uri, $dest->uri, FILE_EXISTS_REPLACE);
 file_save($dest);
 
}

function file_rename_validate($form, &$form_state) {
 $new_name = $form_state['values']['file_operation_name'];
 $old_name = $form_state['file_upload_filename'];
 if (in_array($new_name, array($old_name, ''))) {
  form_set_error('file_operation_name', t('Please set a new name for this File!'));
 }
 $file = $form_state['file_upload_dest'];
 $url_commponents  = pathinfo($file->uri);
 $new_uri = $url_commponents['dirname'] . '/' . $new_name  . '.' . $url_commponents['extension'];
 if (file_exists($new_uri)) {
  form_set_error('file_operation_name', t('The file "@new_name.!extension" exists in File System!', array('@new_name' => $new_name, '!extension' => $url_commponents['extension'])));
 }
 $form_state['file_upload_new_uri'] = $new_uri;
}
function file_rename_submit($form, &$form_state) {
 if (form_get_errors()) return;
 $new_name = $form_state['values']['file_operation_name'];
 $file = $form_state['file_upload_dest'];
 $dest = $form_state['file_upload_new_uri'];
 $form_state['file_upload_dest'] = @file_move($file, $dest, FILE_EXISTS_ERROR);
}
function file_delete_validate($form, &$form_state) {

 $file = $form_state['file_upload_dest'];

}
function file_delete_submit($form, &$form_state) {
 if (form_get_errors()) return;
 $file = $form_state['file_upload_dest'];
 $key = 'file-' . $file->fid;
 $replace_ids = variable_get('file_upload_replace_ids', array());
 if(isset($replace_ids[$key])){
  unset($replace_ids[$key]);
  variable_set('file_upload_replace_ids', $replace_ids);
 }
 
 file_delete($file, TRUE);
}

function file_submit_callback($form, &$form_state) {
 $operation_type = $form_state['values']['file_operation_type'];
 $dest = $form_state['file_upload_dest'];
 $commands = array();
 $commands[] = ajax_command_invoke('#file-operation-form','detach');
 if (in_array($operation_type, array('replace', 'rename'))) {
   if ($operation_type == 'replace') {
    file_unmanaged_delete(file_upload_image_style_path('thumbnail', $dest->uri));
    $replace_ids = variable_get('file_upload_replace_ids', array());
    $key = 'file-' . $dest->fid;
    $replace_ids[$key] = isset($replace_ids[$key])? $replace_ids[$key] + 1 : 0;
    variable_set('file_upload_replace_ids', $replace_ids);
  
   }
   $row = file_upload_view_rows(array($dest->fid => $dest));
   $row_html = theme('table_rows', array('rows' => $row));
   $commands[] = ajax_command_replace('#file-' . $dest->fid, $row_html);

 }elseif ($operation_type == 'delete') { 
   $commands[] = ajax_command_remove('.file-' . $dest->fid);
 }
 $commands[] = ajax_command_html('.message-' . $dest->fid, theme('status_messages'));
 $commands[] = ajax_command_invoke('.message-' . $dest->fid, 'delay', array('3000'));
 $commands[] = ajax_command_invoke('.message-' . $dest->fid, 'hide', array('15000'));
 return array('#type' => 'ajax', '#commands' => $commands);
}



function file_upload_uri_addition_form($form, &$form_state) {
    $schema = array_keys(file_get_stream_wrappers());
    $schema = array_diff($schema,array('theme', 'module', 'library', 'temporary'));
    $schema = array('auto' => t('Auto')) + drupal_map_assoc($schema);
    $theme_default = variable_get('theme_default', '');
    $description = t('There are two stream wrapper have created behind the scene, They are "module://" and "theme://". <br/>
     Usage example: The URI "theme://!theme/images" direct the "images" folder of the "!theme" theme , the "!theme" should be the <em>machine name</em> of the theme.</br>
      The above option "Auto" means that whether to use module or theme stream wrapper will auto detect by the first word of the path you giving. Of course you can set stream wrapper to public or other forcely.', array('!theme' => $theme_default));

  $form['uri_add'] = array(
     '#type' => 'fieldset',
     '#title' => t('Add Uri'),
     '#attributes' => array('id' => 'file-upload-path-add'),
     'schema' => array(
      '#type' => 'select',
      '#options' => $schema,
      '#default_value' => 'auto',
     ),
  'path' => array(
   '#type' => 'textfield',
   '#default_value' => '',
   '#attributes' => array('placeholder' => $theme_default . '/images', 'id' => 'file_upload_path'),
   
  ),
  'check_default' => array(
   '#type' => 'checkbox',
   '#title' => t('Set Default'),
   '#states' => array(
    'invisible' => array(
     '#file_upload_path' => array('empty' => TRUE),
    ),
   ),
  ),
  'description' => array(
    '#type' => 'markup',
    '#markup' => '<div>' . $description . '</div>',
  ),
  'submit' => array(
    '#type' => 'submit',
    '#value' => t('Save'),
  ),
);


 
    
 return $form;
}
function file_upload_uri_addition_form_submit($form, &$form_state) {

 
 $path = trim($form_state['values']['path'], "\\/ \t\n\r\0\x0B");
 if (empty($path)) return TRUE;
 $uris = variable_get('file_upload_path',array());
 $schema = $form_state['values']['schema'];
 $check = $form_state['values']['check_default'];
 $schema = $schema == 'auto'? SystemStreamWrapper::detectSchemeByTarget($path) : $schema;
 $uri = $schema . '://' . $path;
 $key = drupal_base64_encode($uri);
 if(!is_dir($uri)){
  drupal_mkdir($uri, NULL, TRUE);
 }
 file_create_htaccess($uri, FALSE);
 $uris[$key] = array('schema' => $schema, 'path' => $path);
 variable_set('file_upload_path', $uris);
 if ($check || count($uris) == 1) {
  variable_set('file_upload_path_default', $key);
 }
}

function file_upload_path_delete($key) {
    $uris = variable_get('file_upload_path', array());
    if (!empty($uris)) {
	   unset($uris[$key]);
	   variable_set('file_upload_path', $uris);
    }
    $commands[] = ajax_command_remove('.path-row-' . $key);
   return array('#type' => 'ajax', '#commands' => $commands);
  
}

function file_upload_upload_form_submit(&$form, &$form_state) {



  $id = variable_get('file_upload_usage_id', 1);
 if ($form_state['values']['file_upload_fid'] != 0) {
  $file = file_load($form_state['values']['file_upload_fid']);
  $file->status = FILE_STATUS_PERMANENT;
  file_save($file);
  file_usage_add($file,'file_upload','file_upload', $id);

 }
 variable_set('file_upload_usage_id', ++$id);


}


function file_upload_config($form, &$form_state){
  $form = array(
    'file_upload_extensions' => array(
        '#title' => t('Allow file extensions'),
        '#type' => 'textfield',
        '#default_value' => variable_get('file_upload_extensions', 'jpg, jpeg, png, gif, bmp'),
        '#value_callback' => 'file_upload_extensions_value',
      ),
    );
  return system_settings_form($form);
}
function file_upload_extensions_value($element, $input = FALSE, $form_state = array()) {
  
  if ($input === FALSE) {
    return isset($element['#default_value']) ? $element['#default_value'] : '';
  }
  $input = trim($input, ' \t\n\r\0,');
  if(strpos($input, ',') && strpos($input, ' ')){
     $input = preg_replace('/[, ]+/', ', ', $input);
  }
  return $input;
}


