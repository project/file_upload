<?php 

function file_upload_view_rows($files) {
 if (!is_array($files)) $files = array($files);
 $rows = array();
 foreach ($files as $fid => $file) {
  $username = user_load($file->uid)->name;
  $url = file_create_url($file->uri);
  $url_with_key = url($url, array('query' => array('r' => drupal_random_key(8)) ));
  $link = l($url, $url_with_key, array('attributes' => array('target' => '_blank', 'class' => array('link-' . $fid, 'link')  ) ));
  $msg = '<div class="message-' . $fid . '"></div>';
  $link = array('data' => $msg . $link, 'class' => array('url-' . $fid));
  $previewable = preg_match('/image\/.+/', $file->filemime);
  $path = $previewable? $file->uri : 'module://file_upload/asset/file.png';
  $thumbnail = theme('image_style', array('style_name' => 'thumbnail', 'path' => $path, 'width' => '100px', 'height' => '100px', 'attributes' => array('class' => array('thumbnail', 'thumbnail-' . $fid) ), 'usage' => 'file-upload-row', 'data_fid' => $fid ));
  $filesize = format_size($file->filesize);
  $time = format_date($file->timestamp, 'short');
  $operations = array('delete' => t('Delete'), 'rename' => t('Rename'), 'replace' => t('Replace'));
    foreach ( $operations as $key => $value) {
   $attributes = array('class' => array('operation'), 'data_fid' => $fid, 'data_action_type' => $key);
   $operations[$key] = l($value, request_path() , array('attributes' => $attributes, 'fragment' => 'file-' . $fid));
    }
    $operations = implode('<br/>', $operations);
  $rows[] = array( 'data' => array($thumbnail, $username, $filesize, $time, $link, $operations), 'id' => 'file-' . $fid, 'data_fid' => $fid, 'class' => array('file-' . $fid, 'row'));
 }
   return $rows;
 }